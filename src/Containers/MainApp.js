import React, { Component } from 'react';
import { Table, Container, Row, Col, Button } from 'react-bootstrap';
import { LineChart } from 'react-chartkick'
import Form from 'react-bootstrap/Form'
import Collapsible from 'react-collapsible';
import { ChartTheme, ButtonTheme, TopMenuTheme, AccordionTheme, MainBorder, Border, ShopTheme } from '../Theme/Theme.js';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import 'chart.js'
import Alert from 'react-bootstrap/Alert'
import ImageResponsive from 'react-image-responsive'
import badges1 from '../img/img1.png'
import badges2 from '../img/img2.jpg'


import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';

class MainApp extends Component {

    state = {

        disabledButton: false,

        showExtInput: false,

        selectedOption: "all",
        
        projectDetails: {
            name: "",
            pm: "",
            members: ""
        },

        user: {
            id: 0,
            login: "John1234",
            email: "John@mail.com",
            password: "password",
            name: "John",
            lastName: "Smith",
            cash: "10,000,000.00 SVC"
        },

        amount: 0,

        portfolio: [ { "equityId": 0, "count": 30},
                    { "equityId": 1, "count": 3}
        ],

        observedEquities: [ { "equityId": 0},
            { "equityId": 2}
        ],

        equities: [ { "id": 0, "name":"ADJ", "count": 100, "initialPrice": 100, "currentPrice": 80, "initalDate": "1.01.2020", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "12.12.2020", pm: "AAA BBBB", members: 10 },
                  { "id": 1, "name":"VHV", "count": 150, "initialPrice": 200, "currentPrice": 120, "initalDate": "4.01.2020", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "14.12.2021", pm: "CCC DDDD", members: 34},
                  { "id": 2, "name":"GoQu", "count": 300, "initialPrice": 80, "currentPrice": 100, "initalDate": "1.010.2019", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "5.07.2020", pm: "EEE FFFF", members: 5 },
        ],

        equitiesToShow: [ { "id": 0, "name":"ADJ", "count": 100, "initialPrice": 100, "currentPrice": 80, "initalDate": "1.01.2020", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "12.12.2020", pm: "AAA BBBB", members: 10 },
                { "id": 1, "name":"VHV", "count": 150, "initialPrice": 200, "currentPrice": 120, "initalDate": "4.01.2020", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "14.12.2021", pm: "CCC DDDD", members: 34},
                { "id": 2, "name":"GoQu", "count": 300, "initialPrice": 80, "currentPrice": 100, "initalDate": "1.010.2019", "currentTimeStamp": "22.01.2020", "finalTimeStamp": "5.07.2020", pm: "EEE FFFF", members: 5 },
        ],

        equitiesHistory: [ 
            {"id": 0, "prices": {"1-01-2019": 100,
                "2.01.2019": 100,
                "3.01.2019": 105,
                "4.01.2019": 107,
                "5.01.2019": 108,
                "6.01.2019": 110,
                "7.01.2019": 120,
                "8.01.2019": 90,
                "9.01.2019": 85,
                "10.01.2019": 65,
                "11.01.2019": 55,
                "12.01.2019": 60,
                "13.01.2019": 75,
                "14.01.2019": 78,
                "15.01.2019": 78,
                "16.01.2019": 76,
                "17.01.2019": 80,
                "18.01.2019": 78,
                "19.01.2019": 74,
                "20.01.2019": 80,
                "21.01.2019": 80,
                "22.01.2019": 85,
                "23.01.2019": 84,
                "24.01.2019": 83,
                "25.01.2019": 82,
                "26.01.2019": 89,
                "27.01.2019": 92,
                "28.01.2019": 90,
                "29.01.2019": 65,
                "30.01.2019": 78,
                "31.01.2019": 80 }
            },

            { "id": 1, "prices": {"1-01-2019": 10,
                "2.01.2019": 15,
                "3.01.2019": 20,
                "4.01.2019": 30,
                "5.01.2019": 35,
                "6.01.2019": 50,
                "7.01.2019": 60,
                "8.01.2019": 70,
                "9.01.2019": 80,
                "10.01.2019": 115,
                "11.01.2019": 110,
                "12.01.2019": 100,
                "13.01.2019": 98,
                "14.01.2019": 78,
                "15.01.2019": 90,
                "16.01.2019": 76,
                "17.01.2019": 88,
                "18.01.2019": 87,
                "19.01.2019": 87,
                "20.01.2019": 67,
                "21.01.2019": 65,
                "22.01.2019": 56,
                "23.01.2019": 65,
                "24.01.2019": 76,
                "25.01.2019": 67,
                "26.01.2019": 76,
                "27.01.2019": 76,
                "28.01.2019": 67,
                "29.01.2019": 67,
                "30.01.2019": 67,
                "31.01.2019": 76 },
        },

        { "id": 2, "prices": {"1-01-2019": 45,
            "2.01.2019": 54,
            "3.01.2019": 56,
            "4.01.2019": 34,
            "5.01.2019": 43,
            "6.01.2019": 54,
            "7.01.2019": 45,
            "8.01.2019": 65,
            "9.01.2019": 45,
            "10.01.2019": 45,
            "11.01.2019": 54,
            "12.01.2019": 34,
            "13.01.2019": 43,
            "14.01.2019": 34,
            "15.01.2019": 45,
            "16.01.2019": 56,
            "17.01.2019": 65,
            "18.01.2019": 87,
            "19.01.2019": 87,
            "20.01.2019": 65,
            "21.01.2019": 65,
            "22.01.2019": 56,
            "23.01.2019": 76,
            "24.01.2019": 76,
            "25.01.2019": 67,
            "26.01.2019": 76,
            "27.01.2019": 76,
            "28.01.2019": 67,
            "29.01.2019": 76,
            "30.01.2019": 67,
            "31.01.2019": 76 }
        }],

        id: 0,
        ranking : [ {name: "Arthur", value: 1},
                    {name: "John", value: 2},
                    {name: "Agnes", value: 3},
                    {name: "Brian", value: 4} 
        ],

        dictionary : [ {fullName: "Sollers Virtual Coin", shortcut: "SVC"},
                    
        ]
    }
   
    getData(id) {
        let equityForShow = [];
        this.state.equitiesHistory.map(equity => {
            equity.id === id ? equityForShow = equity : equityForShow = equityForShow;
        })
        return  equityForShow.prices
    }

    ifHave(id) {
        let ifhave = false;
        this.state.portfolio.map(wallet => {
            wallet.equityId === id ? ifhave = true : ifhave = ifhave;
        })
        return ifhave;
    }

    changeChar(id) {
        let tmpDisabledButton = true;
        this.setState({
            id: id
        })

        this.state.portfolio.map(wallet => {         
            id === wallet.equityId ? tmpDisabledButton = false : tmpDisabledButton = tmpDisabledButton
        })

        this.setState({
            disabledButton: tmpDisabledButton
        })
        
    }

    getMyCount(id) {
       
        let myCount = 0;
        this.state.portfolio.map(wallet => {
            wallet.equityId === id ? myCount = wallet.count : myCount = myCount;
        })
        return  myCount;
    }
    
    getNameForChar(id) {
        let name = "";
        this.state.equities.map(equity => {
            equity.id === id ? name = equity.name : name = name;
        })
        return  name;
    }

    setAmount(value) {
        this.setState({
            amount: value
        })
    }

    buyEquity(id) {
        let name = "";
        this.state.equities.map(equity => {
            equity.id === id ? name = equity.name : name = name;
        })
       alert("You bought " + this.state.amount + " equity " + name +"'s !!!");
    }

    sellEquity(id) {
        let name = "";
        this.state.equities.map(equity => {
            equity.id === id ? name = equity.name : name = name;
        })
       alert("Sold " + this.state.amount + " equity " + name +"'s !!!");
    }
    
  
    setOption(changeEvent) {
        this.setState({
            selectedOption: changeEvent
        });


        if ( changeEvent == "all") {
            this.setState({
                equitiesToShow: this.state.equities
            });

        }
        
        if ( changeEvent == "onlyMyEquity") {
            let tmpequitiesToShow = [];

            this.state.equities.map(equity => {

                this.state.portfolio.map(wallet => {
                   
                    equity.id === wallet.equityId ? tmpequitiesToShow.push(equity) : console.log("break");
                })
                
            })

            this.setState({
                equitiesToShow: tmpequitiesToShow
            });
        }


        if ( changeEvent == "observed") {
            let observedEquities = [];

            this.state.equities.map(equity => {

                this.state.observedEquities.map(observedEquity => {
                   
                    equity.id === observedEquity.equityId ? observedEquities.push(equity) : console.log("break");
                })
                
            })

            this.setState({
                equitiesToShow: observedEquities
            });
        }
    }

    updateDetails(id) {
        let newDetails = {};

        this.state.equities.map(equity => {
            if ( equity.id === id ) {
                    newDetails.name =equity.name;
                    newDetails.pm =equity.pm;
                    newDetails.members =equity.members;
            }
        })

        this.setState({
            projectDetails: newDetails
        })
    }

    showExtInput(val) {

        this.setState({
            showExtInput: val
        })
        
    }

    render() {

            const { showExtInput, disabledButton, user, equitiesToShow, projectDetails, dictionary, equities, id, ranking } = this.state;
        return (
        <Container>
            <Row>
                <Col sm={12}>
                    <h3>Login: {user.login}Name: {user.name}    Last name: {user.lastName}    Email: {user.email} </h3>
                </Col>
                <Tabs onSelect={(index, label) => console.log(label + ' selected')}>
                  

        {/*  <Tab label="My equity">
                        <Col sm={12}>
                            <Border>
                                <Row>
                                    <Col sm={4}>
                                        <ShopTheme>
                                            <h3>Free cash: {user.cash}</h3>
                                            <Form>
                                                <Form.Group controlId="buyEquity">
                                                    <Form.Control type="number" placeholder="0" onChange={event => this.setAmount(event.target.value)}/>
                                                </Form.Group>
                                                <Button variant="success" type="submit" onClick={() => this.sellEquity(id)}>
                                                    Sell
                                                </Button>
                                            </Form>
                                        </ShopTheme>
                                    </Col>
                                    <Col sm={8}>
                                        <h2>{this.getNameForChar(id)}</h2>
                                        <LineChart data={this.getData(id)} />
                                    </Col>
                                </Row>
                                <Col sm={12}>
                                    <Table responsive variant="dark">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>My count</th>
                                                <th>Total count</th>
                                                <th>Initial price</th>
                                                <th>Current price</th>
                                                <th>Inital date</th>
                                                <th>Current time stamp</th>
                                                <th>Final time stamp</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {equities.map(equity => { return this.ifHave(equity.id) ? <tr key={equity.name} onClick={() => this.changeChar(equity.id)}>
                                                                        <th>{equity.name}</th>
                                                                        <th>{this.getMyCount(equity.id)}</th>
                                                                        <th>{equity.count}</th>
                                                                        <th>{equity.initialPrice}</th>
                                                                        <th>{equity.currentPrice}</th>
                                                                        <th>{equity.initalDate}</th>
                                                                        <th>{equity.currentTimeStamp}</th>
                                                                        <th>{equity.finalTimeStamp}</th>
                                                                    </tr> : ''}
                                            )}
                                        </tbody>
                                    </Table>
                                </Col>
                            </Border>
                        </Col>
                    </Tab>*/} 

                    <Tab label="Wallet">
                        <Col sm={12}>
                            <Border>
                                <Row>
                                    <Col sm={4}>
                                        <ShopTheme>
                                            <h4>Free cash: {user.cash}</h4>
                                            <Form>
                                                <Form.Group controlId="buyEquity">
                                                <Tabs onSelect={(index, label) => console.log(label + ' selected')}>
                                                    <Tab label="Market">
                                                        <Col sm={12}>
                                                            <Border>
                                                                Amount <Form.Control type="number" placeholder="0" onChange={event => this.setAmount(event.target.value)}/>
                                                            </Border>
                                                        </Col>
                                                    </Tab>
                                                    <Tab label="Limit">
                                                        <Col sm={12}>
                                                            <Border>
                                                                Amount<Form.Control type="number" placeholder="0"/>
                                                                    <br></br>
                                                                Limit price<Form.Control type="number" placeholder="0"/>
                                                            </Border>
                                                        </Col>
                                                    </Tab>
                                                </Tabs>
                                                   
                                                </Form.Group>
                                                <Button variant="success" type="submit" onClick={() => this.buyEquity(id)}>
                                                    Buy
                                                </Button>
                                                <Button variant="danger" type="submit" onClick={() => this.sellEquity(id)} disabled={disabledButton}>
                                                    Sell
                                                </Button>
                                            </Form>
                                        
                                            <form>
                                                <ul>
                                                    <li>
                                                        <div className="radio">
                                                            <label>
                                                                <input type="radio" value="all" checked={this.state.selectedOption === 'all'} onChange={event => this.setOption(event.target.value)}/>
                                                                <span className="radioLabel">All equity</span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="radio">
                                                            <label>
                                                                <input type="radio" value="onlyMyEquity" checked={this.state.selectedOption === 'onlyMyEquity'} onChange={event => this.setOption(event.target.value)}/>
                                                                <span className="radioLabel">Only my equity</span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="radio">
                                                            <label>
                                                                <input type="radio" value="observed" checked={this.state.selectedOption === 'observed'} onChange={event => this.setOption(event.target.value)}/>
                                                                <span className="radioLabel">Observed</span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>


                                               
                                            </form>
                
                                        </ShopTheme>
                                    </Col>
                                    <Col sm={8}>
                                        <h2>{this.getNameForChar(id)}</h2>
                                        <LineChart data={this.getData(id)} />
                                    </Col>
                                </Row>
                                <Col sm={12}>
                                    <Table responsive variant="dark">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>My count</th>
                                                <th>Total count</th>
                                                <th>Initial price</th>
                                                <th>Current price</th>
                                                <th>Inital date</th>
                                                <th>Current time stamp</th>
                                                <th>Final time stamp</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {equitiesToShow.map(equity => <tr key={equity.name} onClick={() => this.changeChar(equity.id)}>
                                                                        <th>{equity.name}</th>
                                                                        <th>{this.getMyCount(equity.id)}</th>
                                                                        <th>{equity.count}</th>
                                                                        <th>{equity.initialPrice}</th>
                                                                        <th>{equity.currentPrice}</th>
                                                                        <th>{equity.initalDate}</th>
                                                                        <th>{equity.currentTimeStamp}</th>
                                                                        <th>{equity.finalTimeStamp}</th>
                                                                    </tr>
                                            )}
                                        </tbody>
                                    </Table>
                                </Col>
                            </Border>
                        </Col>
                    </Tab>
                    
                    <Tab label="Ranking">
                        <Col sm={12}>
                            <Border>
                                <Table striped="true" bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Rank (565) </th>   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {ranking.map(user => <tr>
                                                                <th>{user.name}</th>
                                                                <th>{user.value}</th>
                                                            </tr>
                                        )}
                                    </tbody>
                                </Table>
                            </Border>
                        </Col>           
                    </Tab>
                    
                    
                    <Tab label="Badges">
                        <Col sm={12}>
                            <Border>
                                <Table striped="true" bordered hover>
                                        <tbody>
                                            <tr>
                                                <th>Badge</th>
                                                <th>Description</th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <img src={badges1} />
                                                </th>
                                                <th>
                                                <Alert variant="success">
                                                    <Alert.Heading>Top Seller of the week</Alert.Heading>
                                                    <p>
                                                        20.01.2020
                                                    </p>
                                                    <hr />
                                                    <p className="mb-0">
                                                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, 
                                                    </p>
                                                    </Alert>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>
                                                <img src={badges2} />
                                                </th>
                                                <th>
                                                <Alert variant="success">
                                                    <Alert.Heading>SSE Investor</Alert.Heading>
                                                    <p>
                                                        1.01.2020
                                                    </p>
                                                    <hr />
                                                    <p className="mb-0">
                                                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham
                                                    </p>
                                                    </Alert>

                                                </th>
                                            </tr>
                                        </tbody>
                                    </Table>
                            </Border>
                        </Col>
                    </Tab>

                    <Tab label="Dictionary">
                        <Col sm={12}>
                            <Border>
                                <Table striped="true" bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Name</th>  
                                            <th>Description</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {dictionary.map(word => <tr>
                                                                <th>{word.shortcut}</th>
                                                                <th>{word.fullName}</th>
                                                            </tr>
                                        )}
                                    </tbody>
                                </Table>
                            </Border>
                        </Col>
                    </Tab>
                    
                    <Tab label="Project Details">
                        <Col sm={12}>
                             <Table striped="true" bordered hover>
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <th>{projectDetails.name}</th>
                                        </tr>
                                        <tr>
                                            <th>PM</th>
                                            <th>{projectDetails.pm}</th>
                                        </tr>
                                        <tr>
                                            <th>Members</th>
                                            <th>{projectDetails.members}</th>
                                        </tr>
                                    </tbody>
                                </Table>
                        </Col>
                        <Col sm={12}>
                            <Border>
                                <Table responsive variant="dark">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Total count</th>
                                                <th>Initial price</th>
                                                <th>Current price</th>
                                                <th>Inital date</th>
                                                <th>Current time stamp</th>
                                                <th>Final time stamp</th>
                                            </tr>
                                        </thead>  
                                        <tbody>
                                            {equities.map(equity => <tr key={equity.name} onClick={() => this.updateDetails(equity.id)}>
                                                                        <th>{equity.name}</th>
                                                                        <th>{equity.count}</th>
                                                                        <th>{equity.initialPrice}</th>
                                                                        <th>{equity.currentPrice}</th>
                                                                        <th>{equity.initalDate}</th>
                                                                        <th>{equity.currentTimeStamp}</th>
                                                                        <th>{equity.finalTimeStamp}</th>
                                                                    </tr>
                                            )}
                                        </tbody>
                                    </Table>
                            </Border>
                        </Col>
                    </Tab>

                    <Tab label="News">
                        <Col sm={12}>
                            <Border>
                                News
                            </Border>
                        </Col>
                    </Tab>

                    <Tab label="Discussion">
                        <Col sm={12}>
                            <Border>
                                Discussion
                            </Border>
                        </Col>
                    </Tab>
                </Tabs>
            </Row>
           
        </Container>
        )
    }
}

export  default MainApp