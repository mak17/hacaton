import styled from 'styled-components'


export const ChartTheme = styled.div`
    width: 100%; 
    height: 300px;
`

export const MainBorder = styled.div`
    width: 50%;
    float: left;
   
`
export const ShopTheme = styled.div`
    width: 100%;
    text-align: center;
`

export const Border = styled.div`
    text-align: center;
    padding: 14px;
`



export const AccordionTheme = styled.div`
    width: 50%; 
    height: 300px;
    float:left;
    text-align: center;
`

export const TopMenuTheme = styled.div`
    background: #6c757d;
    text-align: center;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
`

export const ButtonTheme = styled.button`
    box-shadow:inset 0px 1px 0px 0px #bee2f9;
    background:linear-gradient(to bottom, #63b8ee 5%, #468ccf 100%);
    background-color:#63b8ee;
    border-radius:6px;
    border:1px solid #3866a3;
    display:inline-block;
    cursor:pointer;
    color:#14396a;
    font-family:Arial;
    font-size:15px;
    font-weight:bold;
    padding:6px 24px;
    text-decoration:none;
    text-shadow:0px 1px 0px #7cacde;
    width: 100%

    &:hover {
        background:linear-gradient(to bottom, #468ccf 5%, #63b8ee 100%);
	    background-color:#468ccf;
      }

    &:action {
        position:relative;
	    top:1px;
    }
`

export const SideMenuTheme = styled.div`
    background: #6c757d;
    text-align: center;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
`

export const BoxItemTheme = styled.div`
    background: #3333ff;
    text-align: center;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
    width: 100%;
    height: 200px;
    margin-top: 20px;

`

export const ItemSideMenuTheme = styled.div`
    background: #6c757d;
    text-align: center;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
    border 2px solid white;

    &:hover {
        background-color: white;
        color: blue;
        cursor: pointer;
      }
`

export const TimeTheme = styled.div`
    background: #6c757d;
    text-align: center;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
    border 2px solid white;
    margin-top: 20px;
    margin-bottom: 20px;
`