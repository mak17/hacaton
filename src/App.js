import React from 'react';
import MainApp from './Containers/MainApp.js'
import './App.css';

function App() {
  return (
    <MainApp />
  );
}

export default App;
